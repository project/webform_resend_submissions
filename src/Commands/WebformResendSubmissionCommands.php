<?php

namespace Drupal\webform_resend_submissions\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\webform\Entity\WebformSubmission;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WebformResendSubmissionCommands extends DrushCommands {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $db;

  /**
   * Indicating if we want to run the code withouth executing the WF handler.
   *
   * Also know as not sending out the emails most likely.
   *
   * @var bool
   */
  private $debugRun;

  /**
   * @var bool
   *
   * If TRUE, check the webform handler conditions before execution.
   */
  private $checkConditions;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   *
   * The language manager service.
   */
  private LanguageManagerInterface $languageManager;

  /**
   * @var int
   *
   * Amount of time (in seconds) to wait between mail sendings.
   */
  private $sleep;

  /**
   * Construct a DiagnosticsCommand object.
   *
   * @param \Drupal\Core\Database\Connection $databaseConnection
   *   The database connection.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(Connection $databaseConnection, LanguageManagerInterface $languageManager) {
    parent::__construct();
    $this->db = $databaseConnection;
    $this->languageManager = $languageManager;
  }

  /**
   * Get all submissions between specific date time stamps and resend.
   *
   * @param string $startTimestamp
   *   A timestamp to start looking from.
   * @param string $endTimestamp
   *   A timestamp to check until.
   * @param string $messageHandlerID
   *   A webform handler to do the resending on.
   *   To provide mulitple handlers, use a comma separated list of handers.
   * @param string $webformId
   *   Filter on a specific webform. If empty just checks all webforms.
   * @param string $checkConditions
   *   If TRUE, check the webform handler conditions before execution.
   * @param int $sleep
   *   Amount of time (in seconds) to wait between mail sendings.
   * @param $debugRun
   *   Run the function without resending the emails.
   *
   * @command pf:resend-submissions
   */
  public function import($startTimestamp, $endTimestamp, $messageHandlerID, $webformId = NULL, $checkConditions = FALSE, $sleep = 0, $debugRun = FALSE) {
    if (empty($startTimestamp) || empty($endTimestamp) || empty($messageHandlerID)) {
      $this->output()->writeln('WF Resend submissions: Error no given start or end timestamp or handlerId');
      return;
    }


    $messageHandlerID = array_filter(array_map('trim', explode(',', $messageHandlerID)));

    $this->checkConditions = !($checkConditions === 'FALSE' || $checkConditions === FALSE);
    $this->sleep = (int) $sleep;

    $this->debugRun = ($debugRun === 'FALSE' || $debugRun === FALSE) ? FALSE : TRUE;
    if ($this->debugRun) {
      $this->output()->writeln('WF Resend submissions: running in debug mode');
    }

    $this->output()->writeln('WF Resend submissions: Start checking submissions between: ' . $startTimestamp . ' - ' . $endTimestamp);

    $query = $this->db->select('webform_submission', 'ws')
      ->fields('ws', ['sid', 'langcode'])
      ->condition('created', [$startTimestamp, $endTimestamp], 'BETWEEN')
      ->orderBy('created');
    if (!empty($webformId) && $webformId !== 'NULL') {
      $query->condition('webform_id', $webformId);
    }
    $submissions = $query->execute()->fetchAll();

    if (empty($submissions)) {
      $this->output()->writeln('WF Resend submissions: No submission found.');
    }

    foreach ($submissions as $submission) {
      /** Make sure we resend the submission in the right language. */
      $this->languageManager->setConfigOverrideLanguage($this->languageManager->getLanguage($submission->langcode));
      $webformSubmission = WebformSubmission::load($submission->sid);

      if (empty($webformSubmission)) {
        $this->output()->writeln('WF Resend submissions: Error loading webform submission for sid: ' . $submission->sid);
        continue;
      }

      foreach ($messageHandlerID as $messageHandler) {
        $this->resendEmailForGivenHandler($messageHandler, $webformSubmission);
      }
    }

    $this->output()->writeln('WF Resend submissions: Done resending all submissions');
  }

  /**
   * Internal function to resend the email for a given handler and submission.
   *
   * @param string $handler
   *   Then webform handler ID.
   * @param object $webformSubmission
   *   The webform submission.
   */
  private function resendEmailForGivenHandler($handler, $webformSubmission) {
    try {
      $message_handler = $webformSubmission->getWebform()->getHandler($handler);
      $message = $message_handler->getMessage($webformSubmission);

      if ((($this->checkConditions && $message_handler->checkConditions($webformSubmission)) || !$this->checkConditions) && !$this->debugRun) {
        $message_handler->sendMessage($webformSubmission, $message);
        sleep($this->sleep);
      }
    }
    catch (\Exception $e) {
      $this->output()->writeln('WF Resend submissions: error: ' . $e->getMessage());
    }
    $this->output()->writeln('WF Resend submissions: resend: ' . $webformSubmission->id() . ' for handler: ' . $handler);
  }

}
